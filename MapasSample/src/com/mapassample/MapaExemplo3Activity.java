package com.mapassample;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

import android.app.Activity;
import android.os.Bundle;

public class MapaExemplo3Activity extends MapActivity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        MapView mv = new MapView( this, "KEY_CODE" );
        mv.setStreetView( true );
        mv.setClickable( true );
        MapController mc = mv.getController();
        
        MyPoint p = new MyPoint( 0, 0 );
        mc.animateTo( p );
        mc.setZoom( 16 );
        setContentView( mv );
    }
    
    @Override
    protected boolean isRouteDisplayed() {
    	return false;
    }
}
