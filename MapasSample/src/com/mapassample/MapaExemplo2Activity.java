package com.mapassample;

import android.os.Bundle;
import android.view.KeyEvent;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

public class MapaExemplo2Activity extends MapActivity {
	MapView mv;
	MapController control;
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        mv = new MapView( this, "KEY_CODE" );
        control = mv.getController();
        control.setZoom( 4 );
        setContentView( mv );
    }
    
    @Override
    protected boolean isRouteDisplayed() {
    	return false;
    }
    
    @Override
    public boolean onKeyDown( int keyCode, KeyEvent event ) {
    	if ( keyCode == KeyEvent.KEYCODE_S ) {
    		// Satelite
    		mv.setSatellite( true );
    		mv.setStreetView( false );
    		mv.setTraffic( false );
    		return true;
    	} else if ( keyCode == KeyEvent.KEYCODE_R ) {
    		// StreetView
    		mv.setSatellite( false );
    		mv.setStreetView( true );
    		mv.setTraffic( false );
    		return true;
    	} else if ( keyCode == KeyEvent.KEYCODE_T ) {
    		// Traffic
    		mv.setSatellite( false );
    		mv.setStreetView( false );
    		mv.setTraffic( true );
    		return true;
    	} else if ( keyCode == KeyEvent.KEYCODE_Z ) {
    		control.zoomIn();
    		return true;
    	} else if ( keyCode == KeyEvent.KEYCODE_X ) {
    		control.zoomOut();
    		return true;
    	}
    	return true;
    }
    
    
}
