package com.mapassample;

import android.os.Bundle;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class MapaExemplo1Activity extends MapActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        MapView mv = new MapView( this, "AIzaSyCgpJJ_JEvEnmh6Pk7cSeuExACSP4mtaBQ" );
        setContentView( mv );
    }
    
    @Override
    protected boolean isRouteDisplayed() {
    	return false;
    }
}