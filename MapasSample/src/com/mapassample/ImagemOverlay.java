package com.mapassample;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.widget.Toast;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class ImagemOverlay extends Overlay {
	
	private int cor;
	private Paint paint = new Paint();
	private GeoPoint geoPoint;
	
	public ImagemOverlay( GeoPoint geoPoint, int cor ) {
		this.geoPoint = geoPoint;
		this.cor = cor;
	}
	
	@Override
	public void draw( Canvas canvas, MapView mapView, boolean shadow ) {
		super.draw( canvas, mapView, shadow );
		
		if ( geoPoint != null ) {
			paint.setColor( cor );
			Point p = mapView.getProjection().toPixels( geoPoint, null );
			Bitmap bitmap = BitmapFactory.decodeResource( mapView.getResources(), android.R.drawable.ic_menu_compass );
			RectF r = new RectF( p.x, p.y, p.x + bitmap.getWidth(), p.y + bitmap.getHeight() );
			canvas.drawBitmap( bitmap, null, r, paint);
		}
	}
	
	public boolean onTap( GeoPoint geoPoint, MapView mapView ) {
		Point point = mapView.getProjection().toPixels( this.geoPoint, null );
		RectF recf = new RectF( point.x - 5, point.y - 5, point.x + 5, point.y + 5 );
		
		Point newPoint = mapView.getProjection().toPixels( geoPoint, null );
		boolean ok = recf.contains( newPoint.x, newPoint.y );
		if ( ok ) {
			Toast.makeText( mapView.getContext(), "Clicou no overlay!!", Toast.LENGTH_SHORT ).show();
			return true;
		}
		return super.onTap( geoPoint, mapView );
	}
	
	public void setGeoPoint( GeoPoint geoPoint ) {
		this.geoPoint = geoPoint;
	}

}
