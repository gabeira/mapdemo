package com.mapassample;

import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.ZoomControls;

import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;

public class MapaExemplo4Activity extends MapActivity {
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        MapView mv = new MapView( this, "KEY_CODE" );
        
        ZoomControls zoom = (ZoomControls)mv.getZoomControls();
        zoom.setLayoutParams( 
        		new ViewGroup.LayoutParams( ViewGroup.LayoutParams.FILL_PARENT,
        		                            ViewGroup.LayoutParams.FILL_PARENT )
        );
        zoom.setGravity( Gravity.BOTTOM + Gravity.CENTER_HORIZONTAL );
        mv.addView( zoom );
        mv.displayZoomControls( true );
        
        setContentView( mv );
    }
    
    @Override
    protected boolean isRouteDisplayed() {
    	return false;
    }
}
