package com.mapassample;

import com.google.android.maps.GeoPoint;

public class MyPoint extends GeoPoint {
	
	// Valores em graus * 1E6
	public MyPoint( int latitude, int longitude ) {
		super( latitude, longitude );
	}

	public MyPoint( double latitude, double longitude ) {
		this( ( int )( latitude * 1E6 ), ( int )( longitude * 1E6 ) );
	}

}
